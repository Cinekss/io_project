/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io;

import java.awt.event.ActionEvent;
import java.io.FileWriter;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

/**
 *
 * @author marcinwojciechowski
 */
public class Zapis extends JText {
    final String filename = "text.out";
 
   JPanel panel = new JPanel();
     Action saveAction = new AbstractAction() {
      public void actionPerformed(ActionEvent e) {
        try {
          doSaveCommand(textField, filename);
        } catch (Exception e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        }
      }
    };
   
     public static void doSaveCommand(JTextComponent textComponent, String filename) throws Exception {
    FileWriter writer = null;
    writer = new FileWriter(filename);
    textComponent.write(writer);
    writer.close();
  }
}

