/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io;

import java.awt.event.ActionEvent;
import java.io.FileReader;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

/**
 *
 * @author marcinwojciechowski
 */
public class Ladowanie extends JText{
    final String filename = "text.out";
    
    Action loadAction = new AbstractAction() {
      public void actionPerformed(ActionEvent e) {
        try {
          doLoadCommand(textField, filename);
        } catch (Exception e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        }
      }
    };
      public static void doLoadCommand(JTextComponent textComponent, String filename) throws Exception {
    FileReader reader = null;
    reader = new FileReader(filename);
    textComponent.read(reader, filename);
    reader.close();
  }
}
